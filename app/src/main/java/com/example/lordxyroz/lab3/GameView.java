package com.example.lordxyroz.lab3;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DiscretePathEffect;
import android.graphics.Point;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RectShape;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener2;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameView extends SurfaceView implements Runnable, SensorEventListener2 {

    private boolean isRunning = false;

    private Thread gameThread;
    private SurfaceHolder holder;
    private Vibrator haptic;
    private ToneGenerator sound;

    float display_size_x;
    float display_size_y;

    private Circle ball;
    private SensorManager sensorManager;

    private float accelerometer[] = new float[3];
    private float magnetic[] = new float[3];
    private float rotation[] = new float[9];
    private float radians[] = new float[3];

    private double gravity = 9.81;
    private double max_speed = 25;
    private double last_update;


    public GameView(Context context, Display display) {
        super(context);
        holder = getHolder();
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        Point size = new Point();
        display.getSize(size);
        display_size_x = size.x;
        display_size_y = size.y;

        haptic = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        sound = new ToneGenerator(AudioManager.STREAM_NOTIFICATION, 100);

        ball = new Circle();
    }

    @Override
    public void run() {
        while (isRunning) {
            if (!holder.getSurface().isValid()) {
                continue;
            }

            // update
            double now = System.nanoTime() / 100000000.d;
            double delta_time = now - last_update;

            //Log.d("delta_time", Double.toString(delta_time));
            if (delta_time > 1 / 60d) {
                ball.update(delta_time);
                last_update = now;

                // draw
                Canvas canvas = holder.lockCanvas();
                if (canvas != null) {

                    ball.draw(canvas);
                    holder.unlockCanvasAndPost(canvas);
                }
            }
        }
    }

    public void resume() {
        isRunning = true;
        gameThread = new Thread(this);
        gameThread.start();

        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_FASTEST);
    }

    public void pause() {
        isRunning = false;

        boolean retry = true;
        while (retry) {
            try {
                gameThread.join();
                retry = false;
                sensorManager.unregisterListener(this);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            accelerometer[0] = sensorEvent.values[0];
            accelerometer[1] = sensorEvent.values[1];
            accelerometer[2] = sensorEvent.values[2];
        }
        if (sensorEvent.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            magnetic[0] = sensorEvent.values[0];
            magnetic[1] = sensorEvent.values[1];
            magnetic[2] = sensorEvent.values[2];
        }

        SensorManager.getRotationMatrix(rotation, null, accelerometer, magnetic);
        SensorManager.getOrientation(rotation, radians);
    }

    @Override
    public void onFlushCompleted(Sensor sensor){}

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {}

    public class Circle {
        private ShapeDrawable circle;
        private ShapeDrawable square;

        private int x;
        private int y;
        private int d = 100;

        private double x_vel;
        private double y_vel;

        public Circle() {
            circle = new ShapeDrawable(new OvalShape());
            circle.getPaint().setColor(Color.RED);
            square = new ShapeDrawable(new RectShape());
            square.getPaint().setColor(Color.BLACK);

            x = (int) display_size_x / 2 - d / 2;
            y = (int) display_size_y / 2 - d / 2;

            circle.setBounds(x, y, x+d, y+d);
            square.setBounds(d / 4, d / 4, (int) display_size_x - (d / 4), (int) display_size_y - (d / 4));
        }

        public void update(double delta_time) {
            x_vel = x_vel + (Math.sin(radians[1]) * gravity * delta_time);
            y_vel = y_vel + (Math.sin(radians[2]) * gravity * delta_time);

            if (x_vel > max_speed) x_vel = max_speed;
            if (x_vel < -max_speed) x_vel = -max_speed;
            if (y_vel > max_speed) y_vel = max_speed;
            if (y_vel < -max_speed) y_vel =-max_speed;

            if (x - x_vel <= 0 + (d / 4) || x - x_vel >= display_size_x - d - (d / 4)) {
                x_vel = -x_vel * 0.75d;
                haptic.vibrate(16);
                sound.startTone(ToneGenerator.TONE_PROP_BEEP);
            }
            if (y - y_vel <= 0 + (d / 4) || y - y_vel >= display_size_y - d - (d / 4)){
                y_vel = -y_vel * 0.75d;
                haptic.vibrate(16);
                sound.startTone(ToneGenerator.TONE_PROP_BEEP);
            }

            if (x_vel < 0.001 && x_vel > -0.001) x_vel = 0;
            if (y_vel < 0.001 && y_vel > -0.001) y_vel = 0;

            x -= x_vel;
            y -= y_vel;
        }

        protected void draw(Canvas canvas) {
            canvas.drawColor(Color.WHITE);
            square.draw(canvas);
            circle.setBounds(x, y, x+d, y+d);
            circle.draw(canvas);
        }
    }
}
